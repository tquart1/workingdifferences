package tquart1.workingdifferences;

import android.content.ContentValues;
import android.content.DialogInterface;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

import tquart1.workingdifferences.DatabaseSetup.TaskContractDB;
import tquart1.workingdifferences.DatabaseSetup.TaskDBHelp;

import static tquart1.workingdifferences.R.id.action_add_task;

public class MainActivity extends AppCompatActivity {


    private TaskDBHelp tHelper;
    private ListView tTaskListView;
    private ArrayAdapter<String> tAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        tHelper = new TaskDBHelp(this);
        tTaskListView = (ListView) findViewById(R.id.list_db);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    protected void onStop() {
        super.onStop();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case action_add_task:
                final EditText taskEditText = new EditText(this);
                AlertDialog dialog = new AlertDialog.Builder(this)
                        .setTitle("Do you want to add a new task?")
                        .setMessage("What is the next step?")
                        .setView(taskEditText)
                        .setPositiveButton("Add", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                String task = String.valueOf(taskEditText.getText());
                                SQLiteDatabase db = tHelper.getWritableDatabase();
                                ContentValues values = new ContentValues();
                                values.put(TaskContractDB.TaskEntry.COL_TASK_TITLE, task);
                                db.insertWithOnConflict(TaskContractDB.TaskEntry.TABLE, null, values, SQLiteDatabase.CONFLICT_REPLACE);
                                db.close();
                                updateUI();
                            }
                        })
                        .setNegativeButton("Cancel", null)
                        .create();
                dialog.show();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }


    public void deleteTask(View view) {
        View parent = (View) view.getParent();
        TextView taskTextView = (TextView) parent.findViewById(R.id.task_title);
        String task = String.valueOf(taskTextView.getText());
        SQLiteDatabase db = tHelper.getWritableDatabase();

        db.delete(TaskContractDB.TaskEntry.TABLE,
                TaskContractDB.TaskEntry.COL_TASK_TITLE + " = ?",
                new String[]{task});
        db.close();
        updateUI();
    }

    private void updateUI() {
        ArrayList<String> taskList = new ArrayList<>();
        SQLiteDatabase db = tHelper.getReadableDatabase();
        Cursor cursor = db.query(TaskContractDB.TaskEntry.TABLE,
                new String[]{TaskContractDB.TaskEntry._ID, TaskContractDB.TaskEntry.COL_TASK_TITLE},
                null, null, null, null, null);
        while (cursor.moveToNext()) {
            int idx = cursor.getColumnIndex(TaskContractDB.TaskEntry.COL_TASK_TITLE);
            taskList.add(cursor.getString(idx));
        }

        if (tAdapter == null) {
            tAdapter = new ArrayAdapter<>(this, R.layout.task_list_db, R.id.task_title,
                    taskList);
            tTaskListView.setAdapter(tAdapter);
        } else {
            tAdapter.clear();
            tAdapter.addAll(taskList);
            tAdapter.notifyDataSetChanged();
        }

        cursor.close();
        db.close();
    }
}

