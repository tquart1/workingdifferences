package tquart1.workingdifferences.DatabaseSetup;

import android.content.ContentResolver;
import android.net.Uri;
import android.provider.BaseColumns;

/**
 * Created by user on 3/25/2017.
 */

public class TaskContractDB {

    public static final String DB = "com.tquart1.workingdifferences.db";
    public static final int DB_VERSION = 1;
    public static final String TABLE = "tasks";

    public static final String AUTHORITY = "tquart1.cool.MainActivity";
    public static final Uri CONTENT_URI = Uri.parse("content://"+AUTHORITY+"/"+ TABLE);

    public static final int TASKS_LIST = 1;
    public static final int TASKS_ITEM = 2;

    public static final String CONTENT_TYPE = ContentResolver.CURSOR_DIR_BASE_TYPE + "/example.tasksDB/"+TABLE;
    public static final String CONTENT_ITEM_TYPE = ContentResolver.CURSOR_ITEM_BASE_TYPE + "/example/tasksDB" + TABLE;

    public class TaskEntry implements BaseColumns {
        public static final String TABLE = "tasks";

        public static final String COL_TASK_TITLE = "title";
    }
    public class Columns {
        public static final String TASK = "task";
        public static final String _ID = BaseColumns._ID;
    }


}
