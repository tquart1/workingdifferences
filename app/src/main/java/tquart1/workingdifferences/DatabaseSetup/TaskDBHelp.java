package tquart1.workingdifferences.DatabaseSetup;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by user on 3/25/2017.
 */

public class TaskDBHelp extends SQLiteOpenHelper {


    public TaskDBHelp(Context context) {
        super(context, TaskContractDB.DB, null, TaskContractDB.DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String createTable = "CREATE TABLE " + TaskContractDB.TaskEntry.TABLE + " ( " +
                TaskContractDB.TaskEntry._ID + " INTEGER PRIMARY KEY INCREMENT, " +
                TaskContractDB.TaskEntry.COL_TASK_TITLE + " TEXT NOT EMPTY);";

        db.execSQL(createTable);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TaskContractDB.TaskEntry.TABLE);
        onCreate(db);
    }


}

